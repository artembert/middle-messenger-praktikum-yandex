import { registerSwitchCaseHelper } from './switch-case/switch-case.helper';

export function registerHelpers(): void {
  registerSwitchCaseHelper();
}
