export const accountHeaderTemplate = `
<div class="account-header window__title">
  <h1 class="account-header__title title">
    {{title}}
  </h1>
  <div class="account-header__avatar">
    {{> app-avatar size='L'}}
  </div>
</div>
`;
