export const Routes = {
  INDEX: '/',
  CHATS: '/chats',
  SIGN_IN: '/sign-in',
  REGISTER: '/register',
  ACCOUNT: '/account',
  EDIT_ACCOUNT: '/edit-account',
  CHANGE_PASSWORD: '/change-password',
  NOT_FOUND: '/404',
  INTERNAL_ERROR: '/500',
};
