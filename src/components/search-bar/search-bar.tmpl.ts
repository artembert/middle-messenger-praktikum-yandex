export const searchBarTemplate = `
<div class="search-bar">
  <input
    class="search-bar__field input-field"
    type="search"
    name="{{name}}"
    value="{{value}}"
    placeholder="{{placeholder}}"
  />
</div>
`;
