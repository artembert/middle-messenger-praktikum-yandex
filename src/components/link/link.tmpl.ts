export const linkTemplate = `
<a href="{{href}}" class="button button_mode_{{mode}}" tabindex="0">
  {{text}}
</a>
`;
